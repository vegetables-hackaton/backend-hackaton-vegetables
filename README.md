## Hackathon - Entering to rooms

<hr>
### Running:

    1) Running by jar download Backend-Hackaton-Vegetables.jar and paste command: 

`java -jar Backend-Hackaton-Vegetables.jar`

<hr>

### Swagger

#### Swagger is an open source set of rules, specifications and tools for developing and describing RESTFUL APIs. The Swagger framework allows developers to create interactive, machine and human-readable API documentation.

##### Bank app API:

![API for bank](src/main/resources/image/swagger.png?raw=true "Title")

<hr>

### Commits:

#### Commit types

| **Emoji** | **Commit type** |
|:---------:|:---------------:|
|    🔥     |  Major update   |
|    💧     |  Minor update   |
|    👾     |       Fix       |
|    🐣     |       Add       |
|     ✂     |    Refactor     |
|    🥂     |     Finish      |